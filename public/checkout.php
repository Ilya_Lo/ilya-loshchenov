<?php require_once("../resources/config.php"); ?>
<?php require_once("cart.php"); ?>
<?php include(TEMPLATE_FRONT . DS . "header.php") ?>




<div class="container">

    <div class="row">
          <h4 class="text-center bg-danger"><?php display_message(); ?></h4>
          <h1>Checkout</h1>

    <form>
        <table class="table">
            <thead>
              <tr>
               <th>Product</th>
               <th>Price</th>
               <th>Quantity</th>
               <th>Sub-total</th>

              </tr>
            </thead>
            <tbody>
                <?php cart(); ?>
            </tbody>
        </table>
    </form>





<div class="col-xs-4">
  <h2>Items Total</h2>

 <table class="table">

    <tr>
        <th>Items:</th>
          <td><span><?php

          echo isset($_SESSION['item_quantity']) ? $_SESSION['item_quantity'] : $_SESSION['item_quantity'] = "0";
          ?></span></td>
    </tr>
    <tr>
      <th>Shipping and Handling</th>
      <td>Free Shipping</td>
    </tr>

    <tr>
      <th>Order Total</th>
      <td><strong><span class="amount">&#36;<?php
       echo isset($_SESSION['item_total']) ? $_SESSION['item_total'] : $_SESSION['item_total'] = "0";
      ?>
      </span></strong> </td>
    </tr>
    </tbody>
  </table>
</div>


  </div>


</div>

<?php include(TEMPLATE_FRONT . DS . "footer.php") ?>
