<?php require_once("../resources/config.php"); ?>

<?php


if(isset($_GET['add'])) {

  $query = query("SELECT * FROM products WHERE pro_id=" . escape_string($_GET['add']) . " ");

  confirm($query);


  while($row = fetch_array($query)) {


    if($row['pro_quantity'] != $_SESSION['product_' . $_GET['add']]) {

      $_SESSION['product_' . $_GET['add']]+=1;

      redirect("checkout.php");

    }
    else {

      set_message("We only have " . $row['pro_quantity'] . " " . "{$row['pro_title']}" . "avaliable");
      redirect("checkout.php");

    }

  }
}


if(isset($_GET['remove'])) {

  $_SESSION['product_' . $_GET['remove']]--;

  if($_SESSION['product_' . $_GET['remove']] < 1) {
    unset($_SESSION['item_total']);
    unset($_SESSION['item_quantity']);
    redirect("checkout.php");
  } else {
    redirect("checkout.php");
  }
}

if(isset($_GET['delete'])) {

  $_SESSION['product_' . $_GET['delete']] = 0;

  //unset to 0
  unset($_SESSION['item_total']);
  unset($_SESSION['item_quantity']);

  redirect("checkout.php");
}



function cart() {

// total items
$total = 0;
$item_quantity = 0;

foreach ($_SESSION as $name => $value) {

  //value which is amount
  if($value > 0 ) {

    if(substr($name, 0, 8 ) == "product_") {



      $length = strlen($name - 8);


      $id = substr($name, 8, $length);

      //we need to loop through our database and display the products
      $query = query("SELECT * FROM products WHERE pro_id = " . escape_string($id)." ");
      confirm($query);

      while ($row = fetch_array($query)) {


      $sub = $row['pro_price']*$value;
      $item_quantity +=$value;

      $product = <<<DELIMETER

      <tr>
          <td>{$row['pro_title']}</td>
          <td>&#36;{$row['pro_price']}</td>
          <td>{$value}</td>
          <td>{$sub}</td>
          <td>
          <a class="btn btn-warning" href="cart.php?remove={$row['pro_id']}"><span class="glyphicon glyphicon-minus"></span></a>
          <a class="btn btn-success" href="cart.php?add={$row['pro_id']}"><span class="glyphicon glyphicon-plus"></span></a>
          <a class="btn btn-danger" href="cart.php?delete={$row['pro_id']}"><span class="glyphicon glyphicon-remove"></span></a>
          </td>
      </tr>

DELIMETER;

  echo $product;


        }

  $_SESSION['item_total'] = $total += $sub;
  $_SESSION['item_quantity'] = $item_quantity;

     }



  }



 }

}

 ?>
