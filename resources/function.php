<?php

function set_message($msg) {
  if(!empty($msg)) {
    $_SESSION['message'] = $msg;
  } else {
    $msg = "";
  }
}

function display_message() {
  if(isset($_SESSION['message'])) {
    echo $_SESSION['message'];
    unset($_SESSION['message']);
  }
}

function redirect($location) {

  header("Location: $location ");
}

function query($sql) {

 //you use a globla variable inside the function
  global $connection;

  return mysqli_query($connection, $sql);
}

function confirm($result) {
  global $connection;

  if(!$result) {
    die("QUERY FAILED " . mysqli_error($connection));
  }
}

function escape_string($string) {
  global $connection;

  // to prevent sql injections
  return mysqli_real_escape_string($connection, $string);
}

function fetch_array($result) {
  return mysqli_fetch_array($result);
}


function items() {
  $query = query("SELECT * FROM products");
  //to confirm if it works
  confirm($query);

  while($row = fetch_array($query)) {

$product = <<<DELIMETER

<div class="col-md-4">

    <div class="thumbnail">
        <a href="cart.php?add={$row['pro_id']}"><img src="{$row['pro_image']}" alt=""></a>
        <div class="caption">
            <h4>&#36;{$row['pro_price']}</h4>
            <h4><a href="cart.php?add={$row['pro_id']}">{$row['pro_title']}</a>
            <h5>{$row['pro_name']}</h5>
            </h4>
            <p>{$row['pro_size']}</p>
        </div>

        <a class="btn btn-primary" target="_blank" href="cart.php?add={$row['pro_id']}">Add to cart</a>
    </div>
</div>

<div class="col-md-4">
    <div class="thumbnail">
        <a href="cart.php?add={$row['pro_id']}"><img src="{$row['pro_image']}" alt=""></a>
        <div class="caption">
            <h4>&#36;{$row['pro_price']}</h4>
            <h4><a href="cart.php?add={$row['pro_id']}">{$row['pro_title']}</a>
            <h5>{$row['pro_name']}</h5>
            </h4>
            <p>{$row['pro_size']}</p>
        </div>

        <a class="btn btn-primary" target="_blank" href="cart.php?add={$row['pro_id']}">Add to cart</a>

    </div>
</div>

<div class="col-md-4">
    <div class="thumbnail">
        <a href="cart.php?add={$row['pro_id']}"><img src="{$row['pro_image']}" alt=""></a>
        <div class="caption">
            <h4>&#36;{$row['pro_price']}</h4>
            <h4><a href="cart.php?add={$row['pro_id']}">{$row['pro_title']}</a>
            <h5>{$row['pro_name']}</h5>
            </h4>
            <p>{$row['pro_size']}</p>
        </div>

        <a class="btn btn-primary text-center" target="_blank"  href="cart.php?add={$row['pro_id']}">Add to cart</a>

    </div>
</div>


DELIMETER;

echo $product;
  }
}
?>
